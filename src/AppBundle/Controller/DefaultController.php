<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
		//**************************************REMOVE*************die('working...');
		/*
		 * ptHealth Solution Starts
		 */
		 $enc_msg = array();
		 for ($i = 8; $i < 294; ++$i) {
	
			switch (TRUE){
				case (($i % 3 == 0) and ($i % 5 == 0)):
					//case when a number is divisible by numbers both 3 and 5
					$enc_msg[] = "ptHealth";
					break;
				case (($i % 3 == 0) and ($i % 5 != 0)):
					//case when a number is divisible only by 3
					$enc_msg[] = "pt";
					break;
				case (($i % 3 != 0) and ($i % 5 == 0)):
					//case when a number is divisible only by 5
					$enc_msg[] = "health";
					break;
				default: 
					//case when a number is not divisible by 3 or 5
					$enc_msg[] = $i;
					break;
			}
			
		}
		//Now, we have encrypted message that we can send for our rescue - fingers crossed!
		
		//we pass in prepared array so that no one can see our business logic from the webpage source!
		return $this->render('default/index.html.twig', [
            'message' => $enc_msg,
        ]);
    }
}
