<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_2d5a15b637351316a96762be4651b8c312020af1c893b88c7e68f38248b4cff1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_44df531a7cf635f42751211513822a84654d07042f2f6aea945f92970f79d430 = $this->env->getExtension("native_profiler");
        $__internal_44df531a7cf635f42751211513822a84654d07042f2f6aea945f92970f79d430->enter($__internal_44df531a7cf635f42751211513822a84654d07042f2f6aea945f92970f79d430_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_44df531a7cf635f42751211513822a84654d07042f2f6aea945f92970f79d430->leave($__internal_44df531a7cf635f42751211513822a84654d07042f2f6aea945f92970f79d430_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_64aea65a2085a58cd0f4098056d89d6b60a194970eab7fa4c6cede5dfa2df11e = $this->env->getExtension("native_profiler");
        $__internal_64aea65a2085a58cd0f4098056d89d6b60a194970eab7fa4c6cede5dfa2df11e->enter($__internal_64aea65a2085a58cd0f4098056d89d6b60a194970eab7fa4c6cede5dfa2df11e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('request')->generateAbsoluteUrl($this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_64aea65a2085a58cd0f4098056d89d6b60a194970eab7fa4c6cede5dfa2df11e->leave($__internal_64aea65a2085a58cd0f4098056d89d6b60a194970eab7fa4c6cede5dfa2df11e_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_ddedf466db00e574d290ba37607d02d7bfb5495fa5532af80bb872460a180ca4 = $this->env->getExtension("native_profiler");
        $__internal_ddedf466db00e574d290ba37607d02d7bfb5495fa5532af80bb872460a180ca4->enter($__internal_ddedf466db00e574d290ba37607d02d7bfb5495fa5532af80bb872460a180ca4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_ddedf466db00e574d290ba37607d02d7bfb5495fa5532af80bb872460a180ca4->leave($__internal_ddedf466db00e574d290ba37607d02d7bfb5495fa5532af80bb872460a180ca4_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_904505c375c5062357ce49adf9d361b3fd3b860f6554e18b77f1f2b32eefdd15 = $this->env->getExtension("native_profiler");
        $__internal_904505c375c5062357ce49adf9d361b3fd3b860f6554e18b77f1f2b32eefdd15->enter($__internal_904505c375c5062357ce49adf9d361b3fd3b860f6554e18b77f1f2b32eefdd15_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 12)->display($context);
        
        $__internal_904505c375c5062357ce49adf9d361b3fd3b860f6554e18b77f1f2b32eefdd15->leave($__internal_904505c375c5062357ce49adf9d361b3fd3b860f6554e18b77f1f2b32eefdd15_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@Twig/layout.html.twig' %}*/
/* */
/* {% block head %}*/
/*     <link href="{{ absolute_url(asset('bundles/framework/css/exception.css')) }}" rel="stylesheet" type="text/css" media="all" />*/
/* {% endblock %}*/
/* */
/* {% block title %}*/
/*     {{ exception.message }} ({{ status_code }} {{ status_text }})*/
/* {% endblock %}*/
/* */
/* {% block body %}*/
/*     {% include '@Twig/Exception/exception.html.twig' %}*/
/* {% endblock %}*/
/* */
