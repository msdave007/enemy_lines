<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_a5da7b8cce4f40b62b991528eadaa6c5fd2571612e422d7c3839aa8ca162369e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_345e4760d22a535e99b80c88c872ada83a253fae9d2dc15ce333f37ac74a9215 = $this->env->getExtension("native_profiler");
        $__internal_345e4760d22a535e99b80c88c872ada83a253fae9d2dc15ce333f37ac74a9215->enter($__internal_345e4760d22a535e99b80c88c872ada83a253fae9d2dc15ce333f37ac74a9215_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_345e4760d22a535e99b80c88c872ada83a253fae9d2dc15ce333f37ac74a9215->leave($__internal_345e4760d22a535e99b80c88c872ada83a253fae9d2dc15ce333f37ac74a9215_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_4fe73f697bf5f3634f318b719dc4a94af8d6eb67633a59b5d78ebb6950665e22 = $this->env->getExtension("native_profiler");
        $__internal_4fe73f697bf5f3634f318b719dc4a94af8d6eb67633a59b5d78ebb6950665e22->enter($__internal_4fe73f697bf5f3634f318b719dc4a94af8d6eb67633a59b5d78ebb6950665e22_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_4fe73f697bf5f3634f318b719dc4a94af8d6eb67633a59b5d78ebb6950665e22->leave($__internal_4fe73f697bf5f3634f318b719dc4a94af8d6eb67633a59b5d78ebb6950665e22_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_8293133bb3dfe20ece9025c9b4f168b4e816853b1f4cf93188f07bf40fd1048d = $this->env->getExtension("native_profiler");
        $__internal_8293133bb3dfe20ece9025c9b4f168b4e816853b1f4cf93188f07bf40fd1048d->enter($__internal_8293133bb3dfe20ece9025c9b4f168b4e816853b1f4cf93188f07bf40fd1048d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_8293133bb3dfe20ece9025c9b4f168b4e816853b1f4cf93188f07bf40fd1048d->leave($__internal_8293133bb3dfe20ece9025c9b4f168b4e816853b1f4cf93188f07bf40fd1048d_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_262439598ceae083a9b5c70d6db90a06873aabb1bb39626037f1a4cb67616999 = $this->env->getExtension("native_profiler");
        $__internal_262439598ceae083a9b5c70d6db90a06873aabb1bb39626037f1a4cb67616999->enter($__internal_262439598ceae083a9b5c70d6db90a06873aabb1bb39626037f1a4cb67616999_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('routing')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_262439598ceae083a9b5c70d6db90a06873aabb1bb39626037f1a4cb67616999->leave($__internal_262439598ceae083a9b5c70d6db90a06873aabb1bb39626037f1a4cb67616999_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 13,  67 => 12,  56 => 7,  53 => 6,  47 => 5,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@WebProfiler/Profiler/layout.html.twig' %}*/
/* */
/* {% block toolbar %}{% endblock %}*/
/* */
/* {% block menu %}*/
/* <span class="label">*/
/*     <span class="icon">{{ include('@WebProfiler/Icon/router.svg') }}</span>*/
/*     <strong>Routing</strong>*/
/* </span>*/
/* {% endblock %}*/
/* */
/* {% block panel %}*/
/*     {{ render(path('_profiler_router', { token: token })) }}*/
/* {% endblock %}*/
/* */
