<?php

/* base.html.twig */
class __TwigTemplate_ab1888354ee8db274b3ab9d7f058d3b1b62cc7b64463faf60ab3dd5e9186c53d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_27cfd0a3d9903b82815fbeb9fe0321d3ae3d18abf756fbf6efe14b13c4bcccab = $this->env->getExtension("native_profiler");
        $__internal_27cfd0a3d9903b82815fbeb9fe0321d3ae3d18abf756fbf6efe14b13c4bcccab->enter($__internal_27cfd0a3d9903b82815fbeb9fe0321d3ae3d18abf756fbf6efe14b13c4bcccab_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        ";
        // line 10
        $this->displayBlock('body', $context, $blocks);
        // line 11
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 12
        echo "    </body>
</html>
";
        
        $__internal_27cfd0a3d9903b82815fbeb9fe0321d3ae3d18abf756fbf6efe14b13c4bcccab->leave($__internal_27cfd0a3d9903b82815fbeb9fe0321d3ae3d18abf756fbf6efe14b13c4bcccab_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_53eeba1d77ba8f7c5b5b9aaf02279879ffb4aaa7be577c6703eaa87d5430c1ac = $this->env->getExtension("native_profiler");
        $__internal_53eeba1d77ba8f7c5b5b9aaf02279879ffb4aaa7be577c6703eaa87d5430c1ac->enter($__internal_53eeba1d77ba8f7c5b5b9aaf02279879ffb4aaa7be577c6703eaa87d5430c1ac_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_53eeba1d77ba8f7c5b5b9aaf02279879ffb4aaa7be577c6703eaa87d5430c1ac->leave($__internal_53eeba1d77ba8f7c5b5b9aaf02279879ffb4aaa7be577c6703eaa87d5430c1ac_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_bf1ce7977f3e3721f923b8ba01ae4bf891d51388e719c129fa401f7d253f3b9b = $this->env->getExtension("native_profiler");
        $__internal_bf1ce7977f3e3721f923b8ba01ae4bf891d51388e719c129fa401f7d253f3b9b->enter($__internal_bf1ce7977f3e3721f923b8ba01ae4bf891d51388e719c129fa401f7d253f3b9b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_bf1ce7977f3e3721f923b8ba01ae4bf891d51388e719c129fa401f7d253f3b9b->leave($__internal_bf1ce7977f3e3721f923b8ba01ae4bf891d51388e719c129fa401f7d253f3b9b_prof);

    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        $__internal_13895491d400fbf4753c9b24cd2e40817010a54394968e8497a8c5cbfa66beb2 = $this->env->getExtension("native_profiler");
        $__internal_13895491d400fbf4753c9b24cd2e40817010a54394968e8497a8c5cbfa66beb2->enter($__internal_13895491d400fbf4753c9b24cd2e40817010a54394968e8497a8c5cbfa66beb2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_13895491d400fbf4753c9b24cd2e40817010a54394968e8497a8c5cbfa66beb2->leave($__internal_13895491d400fbf4753c9b24cd2e40817010a54394968e8497a8c5cbfa66beb2_prof);

    }

    // line 11
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_0e97da20b786b4eac08753ff24467ce56eda3dc6b9be4bda12390e9d7a9b94d5 = $this->env->getExtension("native_profiler");
        $__internal_0e97da20b786b4eac08753ff24467ce56eda3dc6b9be4bda12390e9d7a9b94d5->enter($__internal_0e97da20b786b4eac08753ff24467ce56eda3dc6b9be4bda12390e9d7a9b94d5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_0e97da20b786b4eac08753ff24467ce56eda3dc6b9be4bda12390e9d7a9b94d5->leave($__internal_0e97da20b786b4eac08753ff24467ce56eda3dc6b9be4bda12390e9d7a9b94d5_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 11,  82 => 10,  71 => 6,  59 => 5,  50 => 12,  47 => 11,  45 => 10,  38 => 7,  36 => 6,  32 => 5,  26 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*         <meta charset="UTF-8" />*/
/*         <title>{% block title %}Welcome!{% endblock %}</title>*/
/*         {% block stylesheets %}{% endblock %}*/
/*         <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}" />*/
/*     </head>*/
/*     <body>*/
/*         {% block body %}{% endblock %}*/
/*         {% block javascripts %}{% endblock %}*/
/*     </body>*/
/* </html>*/
/* */
